// $(document).ready(function () {
//   $(".image-container img").hover(
//     function () {
//       // Эффект при наведении
//       $(this).css("transform", "scale(1.1)");
//     },
//     function () {
//       // Возвращение к исходному состоянию
//       $(this).css("transform", "scale(1)");
//     }
//   );
// });


// // Поворот изображения
$(document).ready(function() {
  $('.image-container img').hover(
      function() {
          // Эффект поворота при наведении
          $(this).css('transform', 'rotate(10deg)');
      },
      function() {
          // Возвращение к исходному состоянию
          $(this).css('transform', 'rotate(0deg)');
      }
  );
});




// // Изменение прозрачности
// $(document).ready(function() {
//   $('.image-container img').hover(
//       function() {
//           // Эффект изменения прозрачности при наведении
//           $(this).css('opacity', '0.5');
//       },
//       function() {
//           // Возвращение к исходному состоянию
//           $(this).css('opacity', '1');
//       }
//   );
// });




// // Изменение оттенка
// $(document).ready(function() {
//   $('.image-container img').hover(
//       function() {
//           // Эффект изменения оттенка при наведении
//           $(this).css('filter', 'hue-rotate(90deg)');
//       },
//       function() {
//           // Возвращение к исходному состоянию
//           $(this).css('filter', 'hue-rotate(0deg)');
//       }
//   );
// });


// // Размытие изображения
// $(document).ready(function() {
//   $('.image-container img').hover(
//       function() {
//           // Эффект размытия при наведении
//           $(this).css('filter', 'blur(5px)');
//       },
//       function() {
//           // Возвращение к исходному состоянию
//           $(this).css('filter', 'blur(0px)');
//       }
//   );
// });








document.getElementById("generate-btn").addEventListener("click", function () {
  // Получаем введенный текст
  const text = document.getElementById("text-input").value;

  // Создаем новый контейнер для QR-кода
  const qrcodeContainer = document.createElement("div");
  qrcodeContainer.style.display = "inline-block";
  qrcodeContainer.style.margin = "10px";

  // Добавляем новый контейнер в основной контейнер
  document.getElementById("qrcode-container").appendChild(qrcodeContainer);

  // Создаем новый QR-код
  new QRCode(qrcodeContainer, {
    text: text,
    width: 128,
    height: 128,
  });
});
