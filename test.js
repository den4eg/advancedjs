


const AUTH_TOKEN = "Bearer 8efc716e-d772-40d2-92ba-bd3b5c4f32d8";
const API_URL = "https://ajax.test-danit.com/api/v2/cards";

function createAppointment(appointment) {
  fetch(API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: AUTH_TOKEN,
    },
    body: JSON.stringify(appointment),
  })
    // Перетворення відповіді у формат JSON
    .then((response) => {
      response.ok && getAppointmentList();
    })
    // Обробка помилок
    .catch((error) => console.log(error));
}

// Отримання форми для створення запису
const appointmentForm = document.getElementById("appointment-form");
appointmentForm.addEventListener("submit", (event) => {
  event.preventDefault();

  // Отримання значень з полів форми
  const title = document.getElementById("title").value;
  const description = document.getElementById("description").value;
  const doctor = document.getElementById("doctor").value;
  const bp = document.getElementById("bp").value;
  const age = document.getElementById("age").value;
  const weight = document.getElementById("weight").value;

  // Перевірка заповнення всіх полів
  if (title && description && doctor && bp && age && weight) {
    const appointment = { title, description, doctor, bp, age, weight };
    createAppointment(appointment);
    // Очищення форми
    appointmentForm.reset();
  } else {
    alert("Please fill in all fields");
    appointmentForm.reset();
  }
});

// Отримання списку записів
function getAppointmentList() {
  fetch(API_URL, {
    method: "GET",
    headers: {
      Authorization: AUTH_TOKEN,
    },
  })
    .then((response) => response.json())
    .then((appointments) => {
      const appointmentContainer = document.getElementById("appointment-list");
      appointmentContainer.innerHTML = "";
      // Обробка кожного запису
      appointments.forEach((appointment) => {
        const appointmentCard = document.createElement("div");
        appointmentCard.classList = "appointment-card";

        const title = document.createElement("h3");
        title.innerText = appointment.title;
        appointmentCard.append(title);

        const doctorParagraph = document.createElement("p");
        doctorParagraph.innerText = appointment.doctor;
        appointmentCard.append(doctorParagraph);

        const descriptionParagraph = document.createElement("p");
        descriptionParagraph.innerText = appointment.description;
        appointmentCard.append(descriptionParagraph);

        const deleteButton = document.createElement("button");
        deleteButton.classList = "btn delete-btn";
        deleteButton.innerText = "Delete";
        // Додавання події для видалення запису
        deleteButton.addEventListener("click", () =>
          deleteAppointmentById(appointment.id)
        );
        appointmentCard.append(deleteButton);

        const editButton = document.createElement("button");
        editButton.classList = "btn edit-btn";
        editButton.innerText = "Edit";
        // Додавання події для редагування запису
        editButton.addEventListener("click", () => {
          const editModal = document.getElementById("edit-modal");
          const editForm = document.createElement("form");

          const titleInp = document.createElement("input");
          titleInp.id = "new-title";
          titleInp.type = "text";
          titleInp.value = appointment.title;
          editForm.append(titleInp);

          const descriptionInp = document.createElement("input");
          descriptionInp.id = "new-description";
          descriptionInp.type = "text";
          descriptionInp.value = appointment.description;
          editForm.append(descriptionInp);

          const doctorInp = document.createElement("input");
          doctorInp.id = "new-doctor";
          doctorInp.type = "text";
          doctorInp.value = appointment.doctor;
          editForm.append(doctorInp);

          const bpInp = document.createElement("input");
          bpInp.id = "new-bp";
          bpInp.type = "text";
          bpInp.value = appointment.bp;
          editForm.append(bpInp);

          const ageInp = document.createElement("input");
          ageInp.id = "new-age";
          ageInp.type = "number";
          ageInp.value = appointment.age;
          editForm.append(ageInp);

          const weightInp = document.createElement("input");
          weightInp.id = "new-weight";
          weightInp.type = "number";
          weightInp.value = appointment.weight;
          editForm.append(weightInp);

          const submit = document.createElement("button");
          submit.type = "submit";
          submit.innerText = "Submit";
          editForm.append(submit);

          // Обробка події редагування форми
          editForm.addEventListener("submit", (event) => {
            event.preventDefault();
            const newTitle = document.getElementById("new-title").value;
            const newDescription =
              document.getElementById("new-description").value;
            const newDoctor = document.getElementById("new-doctor").value;
            const newBp = document.getElementById("new-bp").value;
            const newAge = document.getElementById("new-age").value;
            const newWeight = document.getElementById("new-weight").value;

            if (
              newTitle &&
              newDescription &&
              newDoctor &&
              newBp &&
              newAge &&
              newWeight
            ) {
              const newAppointment = {
                title: newTitle,
                description: newDescription,
                doctor: newDoctor,
                bp: newBp,
                age: newAge,
                weight: newWeight,
                id: appointment.id,
              };
              editAppointmentById(appointment.id, newAppointment);
              editForm.reset();
              editModal.style.display = "none";
            }
          });

          editModal.append(editForm);
          editModal.style.display = "block";
        });
        appointmentCard.append(editButton);

        appointmentContainer.append(appointmentCard);
      });
    })
    // Обробка помилок
    .catch((error) => console.log(error));
}

// Видалення запису за його ID
function deleteAppointmentById(id) {
  fetch(`${API_URL}/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: AUTH_TOKEN,
    },
  })
    .then((response) => response.status === 200 && getAppointmentList())
    // Обробка помилок
    .catch((error) => console.log(error));
}

// Редагування запису за його ID
function editAppointmentById(id, newAppointment) {
  fetch(`${API_URL}/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: AUTH_TOKEN,
    },
    body: JSON.stringify(newAppointment),
  })
    .then((response) => response.status === 200 && getAppointmentList())
    // Обробка помилок
    .catch((error) => console.log(error));
}

// Отримання списку записів при завантаженні сторінки
getAppointmentList();


























// const AUTH_TOKEN = "Bearer 8efc716e-d772-40d2-92ba-bd3b5c4f32d8";
// const API_URL = "https://ajax.test-danit.com/api/v2/cards";

// function createAppointment(appointment) {
//   fetch(API_URL, {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json",
//       Authorization: AUTH_TOKEN,
//     },
//     body: JSON.stringify(appointment),
//   })
//     // Перетворення відповіді у формат JSON
//     .then((response) => {
//       response.ok && getAppointmentList();
//     })
//     // Обробка помилок
//     .catch((error) => console.log(error));
// }

// // Отримання форми для створення запису
// const appointmentForm = document.getElementById("appointment-form");
// appointmentForm.addEventListener("submit", (event) => {
//   event.preventDefault();

//   // Отримання значень з полів форми
//   const title = document.getElementById("title").value;
//   const description = document.getElementById("description").value;
//   const doctor = document.getElementById("doctor").value;
//   const bp = document.getElementById("bp").value;
//   const age = document.getElementById("age").value;
//   const weight = document.getElementById("weight").value;

//   // Перевірка заповнення всіх полів
//   if (title && description && doctor && bp && age && weight) {
//     const appointment = { title, description, doctor, bp, age, weight };
//     createAppointment(appointment);
//     // Очищення форми
//     appointmentForm.reset();
//   } else {
//     alert("Please fill in all fields");
//     appointmentForm.reset();
//   }
// }

// // Отримання списку записів
// function getAppointmentList() {
//   fetch(API_URL, {
//     method: "GET",
//     headers: {
//       Authorization: AUTH_TOKEN,
//     },
//   })
//     .then((response) => response.json())
//     .then((appointments) => {
//       const appointmentContainet = document.getElementById("appointment-list");
//       appointmentContainet.innerHTML = "";
//       // Обробка кожного запису
//       appointments.forEach((appointment) => {
//         const appointmentCard = document.createElement("div");
//         appointmentCard.classList = "appointment-card";

//         const title = document.createElement("h3");
//         title.innerText = appointment.title;
//         appointmentCard.append(title);

//         const doctorParagraph = document.createElement("p");
//         doctorParagraph.innerText = appointment.doctor;
//         appointmentCard.append(doctorParagraph);

//         const descriptionParagraph = document.createElement("p");
//         descriptionParagraph.innerText = appointment.description;
//         appointmentCard.append(descriptionParagraph);

//         const deleteButton = document.createElement("button");
//         deleteButton.classList = "btn delete-btn";
//         deleteButton.innerText = "Delete";
//         // Додавання події для видалення запису
//         deleteButton.addEventListener("click", () =>
//           deleteAppontmentById(appointment.id)
//         );
//         appointmentCard.append(deleteButton);

//         const editButton = document.createElement("button");
//         editButton.classList = "btn edit-btn";
//         editButton.innerText = "Edit";
//         // Додавання події для редагування запису
//         editButton.addEventListener("click", () => {
//           const editModal = document.getElementById("edit-modal");
//           const editForm = document.createElement("form");

//           const titleInp = document.createElement("input");
//           titleInp.id = "new-title";
//           titleInp.type = "text";
//           titleInp.value = appointment.title;
//           editForm.append(titleInp);

//           const descriptionInp = document.createElement("input");
//           descriptionInp.id = "new-description";
//           descriptionInp.type = "text";
//           descriptionInp.value = appointment.description;
//           editForm.append(descriptionInp);

//           const doctorInp = document.createElement("input");
//           doctorInp.id = "new-doctor";
//           doctorInp.type = "text";
//           doctorInp.value = appointment.doctor;
//           editForm.append(doctorInp);

//           const bpInp = document.createElement("input");
//           bpInp.id = "new-bp";
//           bpInp.type = "text";
//           bpInp.value = appointment.bp;
//           editForm.append(bpInp);

//           const ageInp = document.createElement("input");
//           ageInp.id = "new-age";
//           ageInp.type = "number";
//           ageInp.value = appointment.age;
//           editForm.append(ageInp);

//           const weightInp = document.createElement("input");
//           weightInp.id = "new-weight";
//           weightInp.type = "number";
//           weightInp.value = appointment.weight;
//           editForm.append(weightInp);

//           const submit = document.createElement("button");
//           submit.type = "submit";
//           submit.innerText = "Submit";
//           editForm.append(submit);

//           // Обробка події редагування форми
//           editForm.addEventListener("submit", (event) => {
//             event.preventDefault();
//             const newTitle = document.getElementById("new-title").value;
//             const newDescription =
//               document.getElementById("new-description").value;
//             const newDoctor = document.getElementById("new-doctor").value;
//             const newBp = document.getElementById("new-bp").value;
//             const newAge = document.getElementById("new-age").value;
//             const newWeight = document.getElementById("new-weight").value;

//             if (
//               newTitle &&
//               newDescription &&
//               newDoctor &&
//               newBp &&
//               newAge &&
//               newWeight
//             ) {
//               const newAppointment = {
//                 title: newTitle,
//                 description: newDescription,
//                 doctor: newDoctor,
//                 bp: newBp,
//                 age: newAge,
//                 weight: newWeight,
//                 id: appointment.id,
//               };
//               editAppointmentById(appointment.id, newAppointment);
//               editForm.reset();
//               editModal.style.display = "none";
//             }
//           });

//           editModal.append(editForm);
//           editModal.style.display = "block";
//         });
//         appointmentCard.append(editButton);

//         appointmentContainet.append(appointmentCard);
//       });
//     })
//     // Обробка помилок
//     .catch((error) => console.log(error));
// }

// // Видалення запису за його ID
// function deleteAppontmentById(id) {
//   fetch(`${API_URL}/${id}`, {
//     method: "DELETE",
//     headers: {
//       Authorization: AUTH_TOKEN,
//     },
//   })
//     .then((response) => response.status === 200 && getAppointmentList())
//     // Обробка помилок
//     .catch((error) => console.log(error));
// }

// // Редагування запису за його ID
// function editAppointmentById(id, newAppointment) {
//   fetch(`${API_URL}/${id}`, {
//     method: "PUT",
//     headers: {
//       "Content-Type": "application/json",
//       Authorization: AUTH_TOKEN,
//     },
//     body: JSON.stringify(newAppointment),
//   })
//     .then((response) => response.status === 200 && getAppointmentList())
//     // Обробка помилок
//     .catch((error) => console.log(error));
// }

// // Отримання списку записів при завантаженні сторінки
// getAppointmentList();