const AUTH_TOKEN = "Bearer c812fa3a-e966-4c4f-9bd2-74fc18a0b7a4";
const API_URL = "https://ajax.test-danit.com/api/v2/cards";

export async function getVisitCard() {
  return await fetch(API_URL, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: AUTH_TOKEN,
    },
  })
    .then((response) => response.json())
    .catch((error) => console.log(error));
}

export async function createNewCard(visit) {
  return await fetch(API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: AUTH_TOKEN,
    },
    body: JSON.stringify(visit),
  })
    .then((response) => response.json())
    .catch((error) => console.log(error));
}

export async function editCards(visit, visitId) {
  return await fetch(`https://ajax.test-danit.com/api/v2/cards/${visitId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: AUTH_TOKEN,
    },
    body: JSON.stringify(visit),
  })
    .then((response) => response.json())
    .catch((error) => console.log(error));
}

export async function deleteVisit(visitId) {
  return await fetch(`https://ajax.test-danit.com/api/v2/cards/${visitId}`, {
    method: "DELETE",
    headers: {
      Authorization: AUTH_TOKEN,
    },
  }).catch((error) => console.log(error));
}
