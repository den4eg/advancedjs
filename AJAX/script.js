/*

Теоретичне питання





AJAX (Asynchronous JavaScript and XML) - це технологія, 
що дозволяє веб-сторінкам динамічно взаємодіяти з сервером без 
перезавантаження всієї сторінки. Вона корисна тим, що підвищує користувацький
 досвід, дозволяє ефективно оновлювати контент в реальному часі та покращує 
 продуктивність веб-додатків.


*/

/*
Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.


Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, 
які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
*/






const filmsContainer = document.getElementById('films-container');

// Функція для отримання даних з API
function fetchData(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            callback(JSON.parse(xhr.responseText));
        }
    };
    xhr.send();
}

// Функція для отримання списку персонажів для конкретного фільму
function fetchCharacters(characters, callback) {
    let charactersData = [];
    let count = 0;
    characters.forEach(url => {
        fetchData(url, function(character) {
            charactersData.push(character);
            count++;
            if (count == characters.length) {
                callback(charactersData);
            }
        });
    });
}

// Головна функція для отримання фільмів та персонажів
function getFilmsAndCharacters() {
    fetchData('https://ajax.test-danit.com/api/swapi/films', function(filmsData) {
        filmsData.forEach(film => {
            // Створюємо елемент для відображення інформації про фільм
            const filmElement = document.createElement('div');
            filmElement.className = 'film';
            filmElement.innerHTML = `
                <h2>Episode ${film.episodeId}: ${film.name}</h2>
                <p>${film.openingCrawl}</p>
                <h3>Characters:</h3>
                <ul id="characters-${film.id}">Loading...</ul>
            `;
            filmsContainer.appendChild(filmElement);

            // Отримуємо персонажів для фільму
            fetchCharacters(film.characters, function(characters) {
                const charactersList = document.getElementById(`characters-${film.id}`);
                charactersList.innerHTML = ''; // Очищаємо текст "Loading..."
                characters.forEach(character => {
                    const characterItem = document.createElement('li');
                    characterItem.textContent = character.name;
                    charactersList.appendChild(characterItem);
                });
            });
        });
    });
}

// Викликаємо головну функцію
getFilmsAndCharacters();







