import { preloader } from "./preloader.js";
import { disableScroll } from "./disableScroll.js";
import { Visit, VisitManager } from "./visit.js";
import { _, _qs, _qsa } from "./common.js";
import {
  addCommonFields,
  addSpecificFields,
  createVisitForm,
} from "./fields.js";
import { filterVisits } from "./filter.js";
import { showModal, hideModal } from "./modal.js";
import { updateNoItemsMessage } from "./updateNoItemsMessage.js";
import { createNewCard, editCards, getVisitCard, deleteVisit } from "./api.js";

import {
  modal,
  createCardButton,
  visitForm,
  innerItem,
  closeButton,
} from "./constants.js";
disableScroll();

window.addEventListener("load", preloader);

const visitManager = new VisitManager();

await showVisitCards();

_("btn").addEventListener("click", logIn);
document
  .getElementById("loginForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();

    const username = _("username").value;
    const password = _("password").value;
    const button = _("btn");

    if (username === "test" && password === "test") {
      _("login_inner").classList.add("none");
      _qs(".inner").classList.remove("none");
      _qs(".createInner").classList.remove("none");
      _qs(".video").classList.add("none");
      button.textContent = "Log out";
      document.body.classList.remove("no-scroll");
    } else {
      alert("Invalid login or password");
    }
  });

createCardButton.addEventListener("click", function (event) {
  event.stopPropagation();
  createVisitForm();
  addCommonFields();
  addSpecificFields("cardiologist");
  showModal(modal);
});

window.addEventListener("click", function (event) {
  if (!modal.contains(event.target)) {
    hideModal(modal);
  }
});

if (closeButton) {
  closeButton.addEventListener("click", function () {
    hideModal(modal);
  });
}

updateNoItemsMessage(visitManager);

visitForm.addEventListener("submit", cardVisit);

_("btn__form").addEventListener("click", function (event) {
  event.preventDefault();
  filterVisits();
});

async function showVisitCards() {
  try {
    const visits = await getVisitCard();

    visits.forEach((visit) => {
      visitManager.addVisit(visit);
    });
    updateNoItemsMessage(visitManager);
  } catch (error) {
    console.error("Failed to fetch visit cards:", error);
  }
}

function logIn() {
  const loginInner = _("login_inner");
  const inner = _qs(".inner");
  const createInner = _qs(".createInner");

  const video = _qs(".video");
  const button = _("btn");

  if (button.textContent === "Log In") {
    loginInner.classList.remove("none");
  } else {
    loginInner.classList.add("none");
    inner.classList.add("none");
    video.classList.remove("none");
    button.textContent = "Log In";
    document.body.classList.add("no-scroll");
  }
}

function createVisitCard(data) {
  const visitCard = document.createElement("div");
  visitCard.className = "visitCard";
  visitCard.innerHTML = `
        <select>
            <option value="Open">Open</option>
            <option value="Done">Done</option>
        </select>
        <h3>${data.fullName}</h3>
        <p style="visibility: hidden">Urgency: ${data.urgency}</p>    
        <p>Doctor: ${data.doctor}</p>
        <button class="show-more">Show More</button>
        <button class="edit" data-index="${data.id}">Edit</button>
        <button class="delete-button close" data-index="${data.id}">
        <svg 
        xmlns="http://www.w3.org/2000/svg"
        width="30"
        height="30"
        viewBox="10 40 50 50"
      >
        <path
          d="M 19 15 C 17.977 15 16.951875 15.390875 16.171875 16.171875 C 14.609875 17.733875 14.609875 20.266125 16.171875 21.828125 L 30.34375 36 L 16.171875 50.171875 C 14.609875 51.733875 14.609875 54.266125 16.171875 55.828125 C 16.951875 56.608125 17.977 57 19 57 C 20.023 57 21.048125 56.609125 21.828125 55.828125 L 36 41.65625 L 50.171875 55.828125 C 51.731875 57.390125 54.267125 57.390125 55.828125 55.828125 C 57.391125 54.265125 57.391125 51.734875 55.828125 50.171875 L 41.65625 36 L 55.828125 21.828125 C 57.390125 20.266125 57.390125 17.733875 55.828125 16.171875 C 54.268125 14.610875 51.731875 14.609875 50.171875 16.171875 L 36 30.34375 L 21.828125 16.171875 C 21.048125 15.391875 20.023 15 19 15 z"
        ></path>
      </svg></button>        
    `;

  const editVisitCard = visitCard.querySelector(".edit");
  editVisitCard.addEventListener("click", async function (event) {
    event.stopPropagation();
    const visitId = event.currentTarget.getAttribute("data-index");
    try {
      const visit = visitManager.findById(visitId);
      createVisitForm(visit);
      addCommonFields(visit);
      addSpecificFields(visit.doctor, visit);
      showModal(modal);
    } catch (error) {
      console.error("Failed to edit visit:", error);
    }
  });

  const selectElement = visitCard.querySelector("select");
  selectElement.addEventListener("change", function () {
    if (selectElement.value === "Done") {
      visitCard.style.border = "2px solid blue";
    } else {
      visitCard.style.border = "1px solid rgb(24, 199, 48)";
    }
  });

  const showMoreButton = visitCard.querySelector(".show-more");
  showMoreButton.addEventListener("click", function () {
    let modalWindow = visitCard.querySelector(".windowModel");

    if (!modalWindow) {
      modalWindow = document.createElement("div");
      modalWindow.classList.add("modal");

      modalWindow.innerHTML = `
                <div class="modal__body">
                    <div class="modal__description">
                        <h3>${data.fullName}</h3>
                        <p>Doctor: ${data.doctor}</p>
                        <p>Purpose: ${data.purpose}</p>
                        <p>Description: ${data.description}</p>
                        <p>Urgency: ${data.urgency}</p>
                        ${
                          data.bloodPressure
                            ? `<p>Blood Pressure: ${data.bloodPressure}</p>`
                            : ""
                        }
                        ${data.bmi ? `<p>BMI: ${data.bmi}</p>` : ""}
                        ${
                          data.cardioDiseases
                            ? `<p>Cardio Diseases: ${data.cardioDiseases}</p>`
                            : ""
                        }
                        ${data.age ? `<p>Age: ${data.age}</p>` : ""}
                        ${
                          data.lastVisit
                            ? `<p>Last Visit: ${data.lastVisit}</p>`
                            : ""
                        }
                        <p>Comment: ${data.comment}</p>
                    
                    </div>
                    <button class="modal__close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 72 72">
                            <path d="M 19 15 C 17.977 15 16.951875 15.390875 16.171875 16.171875 C 14.609875 17.733875 14.609875 20.266125 16.171875 21.828125 L 30.34375 36 L 16.171875 50.171875 C 14.609875 51.733875 14.609875 54.266125 16.171875 55.828125 C 16.951875 56.608125 17.977 57 19 57 C 20.023 57 21.048125 56.609125 21.828125 55.828125 L 36 41.65625 L 50.171875 55.828125 C 50.951875 56.608125 51.977 57 53 57 C 54.023 57 55.048125 56.609125 55.828125 55.828125 C 57.390125 54.266125 57.390125 51.733875 55.828125 50.171875 L 41.65625 36 L 55.828125 21.828125 C 57.390125 20.266125 57.390125 17.733875 55.828125 16.171875 C 54.266125 14.609875 51.733875 14.609875 50.171875 16.171875 L 36 30.34375 L 21.828125 16.171875 C 21.048125 15.391875 20.023 15 19 15 z"></path>
                        </svg>
                    </button>
                </div>
            `;

      visitCard.appendChild(modalWindow);

      modalWindow
        .querySelector(".modal__close")
        .addEventListener("click", function () {
          modalWindow.remove();
        });

      const modalBody = modalWindow.querySelector(".modal__body");
      modalBody.addEventListener("click", function (event) {
        event.stopPropagation();
      });
    }

    modalWindow.classList.add("open");
  });

  visitCard
    .querySelector(".delete-button")
    .addEventListener("click", async function (event) {
      const visitId = event.currentTarget.getAttribute("data-index");

      try {
        const visit = visitManager.findById(visitId);
        console.log(visitId, visit);
        await deleteVisit(visit.id);

        visitManager.removeVisit(visit.id);
        visitCard.remove();
        updateNoItemsMessage(visitManager);
      } catch (error) {
        console.error("Failed to delete visit:", error);
      }
    });

  innerItem.appendChild(visitCard);
}

visitManager.getVisitList().forEach((visit) => {
  createVisitCard(visit);
});

async function cardVisit(event) {
  event.preventDefault();

  const formData = new FormData(visitForm);
  const doctor = formData.get("doctor");
  const visit = new Visit(
    doctor,
    formData.get("visitPurpose"),
    formData.get("visitDescription"),
    formData.get("urgency"),
    formData.get("fullName"),
    formData.get("comment")
  );

  if (doctor === "cardiologist") {
    visit.bloodPressure = formData.get("bloodPressure");
    visit.bmi = formData.get("bmi");
    visit.cardioDiseases = formData.get("cardioDiseases");
    visit.age = formData.get("age");
  } else if (doctor === "dentist") {
    visit.lastVisit = formData.get("lastVisit");
  } else if (doctor === "therapist") {
    visit.age = formData.get("age");
  }

  const visitId = formData.get("visitId");
  if (visitId !== null && visitId !== "") {
    const response = await editCards(visit, visitId);
    visitManager.replaceVisit(response, visitId);
  } else {
    const response = await createNewCard(visit);
    visitManager.addVisit(response);
  }

  createVisitCard(visit);

  innerItem.innerHTML = "";

  visitManager.getVisitList().forEach((visit) => {
    createVisitCard(visit);
  });

  updateNoItemsMessage(visitManager);

  visitForm.reset();
  hideModal(modal);
}
