/*

Теоретичне питання

Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.





Робота з зовнішніми даними:
try {
    const data = JSON.parse(jsonString);
    // робота з даними
} catch (error) {
    console.error('Помилка при розборі JSON:', error.message);
}


Робота з мережею:
try {
    const response = await fetch(url);
    const data = await response.json();
    // робота з отриманими даними
} catch (error) {
    console.error('Помилка при запиті даних:', error.message);
}

Робота з файлами:
try {
    const fileContent = fs.readFileSync(filePath, 'utf-8');
    // робота з вмістом файлу
} catch (error) {
    console.error('Помилка при читанні файлу:', error.message);
}


Робота з DOM:
try {
    const element = document.getElementById('someElement');
    element.classList.add('active');
} catch (error) {
    console.error('Помилка при доступі до елементу DOM:', error.message);
}

Конструкція try...catch допомагає зробити код більш стійким до помилок і дозволяє їх обробляти 
без аварійного завершення програми.




*/

/*
Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).

На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).

Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
 
Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.

Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
*/





const wrapper = document.querySelector('.wrapper'); // Отримуємо елемент з класом "wrapper"

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.createElement("div"); // Створюємо новий елемент div
root.id = "root"; // Додаємо до нього атрибут id зі значенням "root"
wrapper.appendChild(root); // Додаємо елемент з id "root" до елементу .wrapper

const ul = document.createElement("ul");

books.forEach((book) => {
  try {
    if (!book.author || !book.name || !book.price) {
      throw new Error(`Incorrect object in array: ${JSON.stringify(book)}`);
    }

    const li = document.createElement("li");
    li.textContent = `${book.name} by ${book.author}, Price: ${book.price}`;
    ul.appendChild(li);
  } catch (error) {
    console.error(error.message);
  }
});

root.appendChild(ul); // Додаємо список до елементу з id "root"







