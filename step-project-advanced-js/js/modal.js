export function showModal(modal) {
  modal.classList.remove("none");
}

export function hideModal(modal) {
  modal.classList.add("none");
}
