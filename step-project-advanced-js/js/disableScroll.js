export function disableScroll() {
  document.body.classList.add("no-scroll");
}
