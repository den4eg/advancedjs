export const _ = (selector) => document.getElementById(selector);
export const _qs = (selector) => document.querySelector(selector);
export const _qsa = (selector) => document.querySelectorAll(selector);