import { _, _qs, _qsa } from "./common.js";

export const modal = _("modalCreateVisit");
export const createCardButton = _qs(".createCard");
export const visitForm = _("visitForm");
export const innerItem = _qs(".innerItem");
export const closeButton = _qs(".modal-content .close");
