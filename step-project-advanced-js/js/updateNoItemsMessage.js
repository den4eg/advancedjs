import { _, _qs, _qsa } from "./common.js";
const noItemsMessage = _qs(".infoCard");
export function updateNoItemsMessage(visitManager) {
  if (visitManager.getVisitList().length > 0) {
    noItemsMessage.classList.add("none");
  } else {
    noItemsMessage.classList.remove("none");
  }
}
