/*

Завдання
Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

Технічні вимоги:
При відкритті сторінки необхідно отримати з сервера список всіх користувачів 
та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні 
дві адреси:


https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts


Після завантаження всіх користувачів та їх публікацій необхідно відобразити 
всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад в папці), 
та включати заголовок, текст, а також ім'я, прізвище та імейл користувача,
 який її розмістив.
На кожній картці повинна бути іконка або кнопка, 
яка дозволить видалити цю картку зі сторінки. 
При натисканні на неї необхідно надіслати DELETE запит на адресу 
https://ajax.test-danit.com/api/json/posts/${postId}.
 Після отримання підтвердження із сервера (запит пройшов успішно), 
 картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API
 можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, 
які надсилалися на сервер, не будуть там збережені. Це нормально, 
все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. 
Для цього необхідно створити клас Card. 
При необхідності ви можете додавати також інші класи.



*/

/*


*/

const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
    this.element = document.createElement("div");
    this.element.className = "card";
    this.element.innerHTML = `
            <img src="${
              post.imageUrl || "https://via.placeholder.com/150"
            }" alt="Post image">
        <div class="cards-items">
            <p ><strong class="user-name">${user.name}</strong>  (${
      user.email
    })</p>
            <h3>${post.title}</h3>
            <p>${post.body}</p>
            
            <span class="delete-btn">&times;</span>
         </div>
        `;
    // Додаємо слухач подій для кнопки видалення
    this.element
      .querySelector(".delete-btn")
      .addEventListener("click", (event) => {
        event.stopPropagation();
        this.deletePost();
      });
    // Додаємо слухач подій для показу інформації про користувача
    this.element.addEventListener("click", () => this.showUserInfo());
  }

  // Метод для видалення поста
  deletePost() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: "DELETE",
    }).then((response) => {
      if (response.ok) {
        this.element.remove();
      } else {
        alert("Failed to delete the post.");
      }
    });
  }

  // Метод для показу інформації про користувача
  showUserInfo() {
    const modal = document.getElementById("modal");
    const userInfo = document.getElementById("user-info");
    userInfo.innerHTML = `
        
            <p><strong>Name:</strong> ${this.user.name}</p>
            <p><strong>Username:</strong> ${this.user.username}</p>
            <p><strong>Email:</strong> ${this.user.email}</p>
            <p><strong>Phone:</strong> ${this.user.phone}</p>
            <p><strong>Website:</strong> ${this.user.website}</p>
            <p><strong>Company:</strong> ${this.user.company.name}</p>
            <p><strong>Address:</strong> ${this.user.address.street}, ${this.user.address.suite}, ${this.user.address.city}, ${this.user.address.zipcode}</p>
            `;
    modal.style.display = "flex";
    document.body.classList.add("no-scroll");
  }

  // Метод для рендеру картки
  render(container) {
    container.appendChild(this.element);
  }
}

// Функція для отримання даних з API
async function fetchData(url) {
  const response = await fetch(url);
  return response.json();
}

// Ініціалізація додатку
async function init() {
  const users = await fetchData(usersUrl);
  const posts = await fetchData(postsUrl);
  const container = document.getElementById("cards-container");

  posts.forEach((post) => {
    const user = users.find((user) => user.id === post.userId);
    if (user) {

      // Додаємо зображення до поста
      post.imageUrl = `https://picsum.photos/300/200?random=${post.id}`;
      const card = new Card(post, user);
      card.render(container);
    }
  });
}

// Додаємо слухач подій для завантаження DOM
document.addEventListener("DOMContentLoaded", () => {
  init();

  const modal = document.getElementById("modal");
  const closeBtn = document.querySelector(".close-btn");

  // Додаємо слухач подій для закриття модального вікна
  closeBtn.addEventListener("click", () => {
    modal.style.display = "none";
    document.body.classList.remove("no-scroll");
  });

  // Закриття модального вікна при кліку за межами вікна
  window.addEventListener("click", (event) => {
    if (event.target === modal) {
      modal.style.display = "none";
      document.body.classList.remove("no-scroll");
    }
  });
});

// Функція для отримання та логування даних
function fetchAndLogData() {
  Promise.all([
    fetch(usersUrl).then((response) => response.json()),
    fetch(postsUrl).then((response) => response.json()),
  ])
    .then(([users, posts]) => {
      console.log("Users:", users);
      console.log("Posts:", posts);
    })
    .catch((error) => {
      console.error("Error fetching data:", error);
    });
}

fetchAndLogData();
