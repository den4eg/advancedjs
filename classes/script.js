/*

Теоретичне питання

1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?



1
Прототипне наслідування в JavaScript – це спосіб організації об'єктно-орієнтованого програмування, 
де об'єкти можуть успадковувати властивості та методи від інших об'єктів. Це відрізняється від класичного наслідування,
 яке використовується в багатьох інших мовах програмування.

2
Виклик super() у конструкторі класу-нащадка використовується для виклику конструктора батьківського класу.
 Це необхідно для того, щоб належним чином ініціалізувати властивості, які визначені в батьківському класі.


*/

/*
Завдання

1.Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.

2.Створіть гетери та сеттери для цих властивостей.

3.Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).

4.Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.

5.Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
*/

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set name(newName) {
    this._name = newName;
  }

  set age(newAge) {
    this._age = newAge;
  }

  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }

  set lang(newLang) {
    this._lang = newLang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer("Alice", 30, 5000, ["JavaScript", "Python"]);
const programmer2 = new Programmer("Bob", 25, 4000, ["Java", "C++"]);

console.log(
  `Name: ${programmer1.name}, Age: ${programmer1.age}, Salary: ${programmer1.salary}, Languages: ${programmer1.lang}`
);

console.log(
  `Name: ${programmer2.name}, Age: ${programmer2.age}, Salary: ${programmer2.salary}, Languages: ${programmer2.lang}`
);
