/*
Теоретичне питання


Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript





Асинхронність у JavaScript дозволяє виконувати завдання паралельно без блокування основного потоку. 
Це важливо для підтримки чутливості веб-додатків під час виконання тривалих операцій, таких як запити до серверів.
 Основні інструменти для роботи з асинхронністю включають:

Callback-функції: Функції, що виконуються після завершення асинхронної операції.

Проміси (Promises): Об'єкти, що представляють майбутнє значення асинхронної операції з методами then і catch для обробки результатів.

async/await: Синтаксичний цукор для роботи з промісами, що дозволяє писати асинхронний код, як синхронний.

*/

/*
Технічні вимоги:
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.

*/

document.addEventListener('DOMContentLoaded', () => {
  const findIpButton = document.getElementById('findIpButton');
  const resultDiv = document.getElementById('result');
  const pirat = document.getElementById('body');

  async function getIpAddress() {
      const response = await fetch('https://api.ipify.org/?format=json');
      if (!response.ok) {
          throw new Error('Network response was not ok');
      }
      const data = await response.json();
      return data.ip;
  }

  async function getLocationInfo(ip) {
      const response = await fetch(`http://ip-api.com/json/${ip}`);
      if (!response.ok) {
          throw new Error('Network response was not ok');
      }
      const data = await response.json();
      console.log("IP Address data:", data);
      return data;
  }

  findIpButton.addEventListener('click', async () => {
      try {
          // Змінюємо фон wrapper на зображення
          pirat.style.backgroundImage = "url('pirates2.jpg')";
          pirat.style.backgroundSize = 'cover';

          const ipAddress = await getIpAddress();
          const locationInfo = await getLocationInfo(ipAddress);

          resultDiv.innerHTML = `
              <p>Континент: ${locationInfo.continent}</p>
              <p>Країна: ${locationInfo.country}</p>
              <p>Регіон: ${locationInfo.regionName}</p>
              <p>Місто: ${locationInfo.city}</p>
              <p>Район: ${locationInfo.district}</p>
          `;
      } catch (error) {
          console.error('Error:', error);
          resultDiv.innerText = 'Не вдалося отримати інформацію';
      }
  });
});
