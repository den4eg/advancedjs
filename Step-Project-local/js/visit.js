export class VisitManager {
    constructor() {
      const storedVisits = JSON.parse(localStorage.getItem("visits")) || [];
      this.visits = storedVisits.map((visit) =>
        Object.assign(new Visit(), visit)
      );
    }
  
    addVisit(visit) {
      this.visits.push(visit);
      this.#store(this.visits);
    }

    replaceVisit(visit, index) {
      this.visits[index] = visit;
      this.#store(this.visits);
   }
  
    removeVisit(index) {
      this.visits.splice(index, 1);
      this.#store(this.visits);
    }
  
    getVisitList() {
      return this.visits;
    }
  
    #store(visits) {
      localStorage.setItem("visits", JSON.stringify(visits));
    }

 }
  
  export class Visit {
    constructor(doctor, purpose, description, urgency, fullName, comment) {
      this.doctor = doctor;
      this.purpose = purpose;
      this.description = description;
      this.urgency = urgency;
      this.fullName = fullName;
      this.comment = comment;
      this.bloodPressure = "";
      this.bmi = "";
      this.cardioDiseases = "";
      this.age = "";
      this.lastVisit = "";
    }
  }