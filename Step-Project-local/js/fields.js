import { _, _qs } from "./common.js";

const formElement = _("visitForm");
export function createVisitForm(data={}, id="") {  
  formElement.innerHTML = "";
  formElement.innerHTML =   
  `<input type="hidden" name="visitId" value="${id}" id="visitId"/>
  <select id="doctorSelect" name="doctor">
  <option value="cardiologist" ${(data.doctor === 'cardiologist') ? 'selected' : ''}>Cardiologist</option>
    <option value="dentist" ${(data.doctor === 'dentist') ? 'selected' : ''}>Dentist</option>
    <option value="therapist" ${(data.doctor === 'therapist') ? 'selected' : ''}>Therapist</option>
  </select>
  <div id="additionalFields"></div>
  <textarea
    id="comment"
    name="comment"
    placeholder="Additional comments"
  >${data.comment ?? ""}</textarea>
  <button class="submitCreate" type="submit">Create</button>
`

const doctorSelect = formElement.querySelector("#doctorSelect");
doctorSelect.addEventListener("change", function () {
  formElement.querySelector("#additionalFields").innerHTML = "";
  if (doctorSelect.value) {
    addCommonFields();
    addSpecificFields(doctorSelect.value);
  }
});
}

export function addCommonFields(data={}) {
  const formElement = _qs(".modal-content");
    formElement.querySelector("#additionalFields").innerHTML =
   `
      <input type="text" id="visitPurpose" name="visitPurpose" value="${data.purpose ?? ""}" placeholder="Purpose of visit" required>
      <textarea id="visitDescription" name="visitDescription" placeholder="Short description" required>${data.description ?? ""}</textarea>
      <select id="urgency" name="urgency" required>
        <option value="normal" ${(data.urgency === 'normal') ? 'selected' : ''}>Normal</option>
        <option value="High" ${(data.urgency === 'High') ? 'selected' : ''}>High</option>
        <option value="Low" ${(data.urgency === 'Low') ? 'selected' : ''}>Low</option>
      </select>
      <input type="text" id="fullName" name="fullName" placeholder="Full name" required value="${data.fullName ?? ""}" >
    `;
  }

  export function addSpecificFields(doctor, data={}) {
    const formElement = _qs(".modal-content");
    switch (doctor) {
      case "cardiologist":
        formElement.querySelector("#additionalFields").innerHTML += `
          <input type="text" id="bloodPressure" name="bloodPressure" placeholder="Blood Pressure" value="${data.bloodPressure ?? ""}"  required>
          <input type="text" id="bmi" name="bmi" placeholder="BMI" value="${data.bmi ?? ""}" required>
          <input type="text" id="cardioDiseases" name="cardioDiseases" placeholder="Cardio diseases history" value="${data.cardioDiseases ?? ""}" required>
          <input type="number" id="age" name="age" placeholder="Age" value="${data.age ?? ""}" required>
        `;
        break;
      case "dentist":
        formElement.querySelector("#additionalFields").innerHTML += `
          <input type="date" id="lastVisit" name="lastVisit" placeholder="Last visit date" value="${data.lastVisit ?? ""}" required>
        `;
        break;
      case "therapist":
        formElement.querySelector("#additionalFields").innerHTML += `
          <input type="number" id="age" name="age" placeholder="Age" value="${data.age ?? ""}" required>
        `;
        break;
    }
  }


