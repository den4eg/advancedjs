import { _, _qs, _qsa } from "./common.js";
export function filterVisits() {
  const searchTitle = _("searchTitle").value.toLowerCase();
  const status = _("status").value;
  const urgencyLevels = _("urgencyLevels").value;

  const visitCards = _qsa(".visitCard");

  visitCards.forEach((card, index) => {
    const titleMatch = card
      .querySelector("h3")
      .textContent.toLowerCase()
      .includes(searchTitle);
    const statusMatch =
      status === "" || card.querySelector("select").value === status;
    const urgencyLevelsMatch =
      urgencyLevels === "" ||
      card
        .querySelector("p:nth-of-type(1)")
        .textContent.toLowerCase()
        .includes(urgencyLevels.toLowerCase());

    if (titleMatch && statusMatch && urgencyLevelsMatch) {
      card.style.display = "";
    } else {
      card.style.display = "none";
    }
  });
}
