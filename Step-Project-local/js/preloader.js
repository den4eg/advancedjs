import { _ } from "./common.js";
export function preloader() {
    setTimeout(function () {
      _("preloader").style.display = "none";    
      _("wrapper").style.display = "block";
    }, 3000); 
  }